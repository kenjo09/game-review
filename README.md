#Game Review

## Technology used
- Dockerized with Nginx and PHP 8.0 FPM
- Symfony 5 for the backend
- Angular 12 for the frontend

## Installation

1. Git clone/copy this repo on your project location.
    ```
    git clone https://gitlab.com/shuridog/game-review.git
    ```
2.  Once done, navigate to game-review-backend folder to update the backend PHP dependencie. Then run composer install.
    ```
    composer install
    ```
3. Angular dependencies/packages should also be updated. Navigate to game-review. Then run npm install.
    _Make sure that you already install node.js and npm before you run the cli._
    ```
    npm install
    ```
4. Navigate back to the root folder where the docker-compose.yml file is located. Then you can now build your docker.
    ```
    docker-compose up -d
    ```

5. Once docker is up, you need now to run the Angular 12 for front-end. Navigate again to game-review then run ng serve. 
    ```
    ng serve
    ```

6. Go to http://localhost:4200/ and it should be now working on your local.

### Author
- 
