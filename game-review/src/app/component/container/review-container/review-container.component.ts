import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { GameReviewService } from 'src/app/service/game-review.service';
import { EventTriggerService } from 'src/app/service/event-trigger.service';

@Component({
  selector: 'app-review-container',
  templateUrl: './review-container.component.html',
  styleUrls: ['./review-container.component.scss']
})
export class ReviewContainerComponent implements OnInit {
  reviewForm = this.formBuilder.group({
    fullName: ['', Validators.required],
    email: ['', Validators.required],
    rating: ['', Validators.required],
    review: ['', Validators.required]
  });

  constructor(
    private formBuilder: FormBuilder, 
    private gameReviewService: GameReviewService,
    private eventTriggerService: EventTriggerService
  ) { }

  ngOnInit(): void {

  }

  changeRating(e: any) {
    this.reviewForm.get('rating')?.setValue(e.target.value, {
      onlySelf: true
   });
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    let params = this.reviewForm.value;

    this.gameReviewService.postGameReview(params).subscribe(hero => this.eventTriggerService.triggerEvent(true));
  }
}
