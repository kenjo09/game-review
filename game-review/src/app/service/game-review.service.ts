import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GameReviewService {
  private apiUrl = 'http://localhost/game-reviews/api';

  constructor(private http: HttpClient) { }

  getGameReview() {
    return this.http.get<any>(this.apiUrl);
  }

  postGameReview(params: any) {
    return this.http.post<any>(this.apiUrl, params);
  }
}

